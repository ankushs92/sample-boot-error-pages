package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import com.sample.errorpages.SampleBootErrorPagesApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SampleBootErrorPagesApplication.class)
@WebAppConfiguration
public class SampleBootErrorPagesApplicationTests {

	@Test
	public void contextLoads() {
	}

}
